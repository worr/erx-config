ANSIBLE_PLAYBOOK := /usr/local/bin/ansible-playbook
ANSIBLE_LINT := /usr/local/bin/ansible-lint

HAUS := ./haus.yaml
ROOT := site.yaml

TARGET ?= HAUS
ANSIBLE_FLAGS ?=
TEMP !!= mktemp -p .

.PHONY: check deploy

check:
	$(ANSIBLE_PLAYBOOK) -i $(HAUS) --syntax-check $(ROOT)
	$(ANSIBLE_LINT) $(ROOT)

deploy:
	$(ANSIBLE_PLAYBOOK) -i $($(TARGET)) $(ROOT) $(ANSIBLE_FLAGS)

dryrun:
	$(ANSIBLE_PLAYBOOK) -C -D -i $($(TARGET)) $(ROOT) $(ANSIBLE_FLAGS)
