# Ansible configs for my custom EdgeRouter X configs

## Configuration

### IPv6 on Orange España

Adds support for requesting a prefix delegation from Orange España and
assigning an IP to our LAN interface, as well as calling the builtin Vyatta
scripts to set up radvd.

We do this largely by """pretending""" we're wide-dhcp6c by creating all the
same lease and pid files that EdgeOS/Vyatta is expecting, and then calling the
scripts that handle the rest of IPv6 configuration.

This is brittle and prone to breakage. Sources are linked in the dhclient
config.

## Checks

The only form of checking is doing

```bash
$ make check
```
